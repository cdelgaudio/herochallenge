//
//  PickableTextfield.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import UIKit

class PickableTextfield: UITextField {

    private var completion: ((String?)->())?
    private var elements: [String] = []

    func setup(elements: [String], completion: ((String?)->())?) {
        let picker = UIPickerView()
        inputView = picker
        self.elements = elements
        picker.delegate = self
        picker.dataSource = self
        self.completion = completion
    }
}

extension PickableTextfield: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return elements.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return elements[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selection = elements[row]
        text = selection
        completion?(selection)
        endEditing(true)
    }

}


