//
//  NetworkService.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import Foundation

class NetworkService {

    enum NetworkError: Error {
        case badURL
    }

    private var session: URLSession = URLSession(configuration: .default)

    static let shared: NetworkService = NetworkService()

    private init() {}

    @discardableResult
    func request<T: Codable>(url: URL, completionHandler: @escaping (Result<T, Error>) -> Void) -> URLSessionDataTask? {
        let request = session.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completionHandler(.failure(error!))
                return
            }
            do {
                completionHandler(.success(try JSONDecoder().decode(T.self, from: data)))
            } catch {
                completionHandler(.failure(error))
            }
        }
        request.resume()
        return request
    }
}
