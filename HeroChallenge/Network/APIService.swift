//
//  APIService.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import Foundation

class APIService {
    @discardableResult
    static func getProducts(completionHandler: @escaping (Result<[Product], Error>) -> Void) -> URLSessionDataTask? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.datakick.org"
        urlComponents.path = "/api/items"
        urlComponents.queryItems = []
        guard let url = urlComponents.url else {
            completionHandler(Result.failure(NetworkService.NetworkError.badURL))
            return nil
        }
        return NetworkService.shared.request(url: url, completionHandler: completionHandler)
    }
}
