//
//  ProductCellPresenter.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import Foundation

class ProductCellPresenter {

    private var product: Product

    init(product: Product) {
        self.product = product
    }

    func getDescription() -> String {
        let dictionary: [Product.DescriptionKey: String?] = [
            .gtin14: product.gtin14,
            .name: product.name,
            .author: product.author,
            .format: product.format?.rawValue,
            .publisher: product.publisher,
            .brandName: product.brandName,
            .size: product.size,
            .servingSize: product.servingSize,
            .servingsPerContainer: product.servingsPerContainer,
            .pages: numberToString(product.pages),
            .calories: numberToString(product.calories),
            .fatCalories: numberToString(product.fatCalories),
            .fat: numberToString(product.fat),
            .saturatedFat: numberToString(product.saturatedFat),
            .transFat: numberToString(product.transFat),
            .polyunsaturatedFat: numberToString(product.polyunsaturatedFat),
            .monounsaturatedFat: numberToString(product.monounsaturatedFat),
            .cholesterol: numberToString(product.cholesterol),
            .sodium: numberToString(product.sodium),
            .carbohydrate: numberToString(product.carbohydrate),
            .fiber: numberToString(product.fiber),
            .sugars: numberToString(product.sugars),
            .protein: numberToString(product.protein),
            .potassium: numberToString(product.potassium)
        ]

        return dictionary.reduce([]) { (result, element) -> [String] in
            guard let value = element.value else { return result }
            return result + ["\(element.key.rawValue.capitalized): \(value)"]
            }.sorted().joined(separator: "\n")
    }

    private func numberToString<T: Numeric>(_ number: T?) -> String? {
        guard let number = number else {
            return nil
        }
        return "\(number)"
    }
}
