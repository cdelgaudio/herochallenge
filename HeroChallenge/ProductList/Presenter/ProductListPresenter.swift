//
//  ProductListPresenter.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import Foundation
protocol ProductListDelegate: class {
    func reloadData()
    func updateButton(direction: OrderDirection)
}

enum OrderDirection: String {
    case ascending, descending
}

class ProductListPresenter {

    private weak var viewDelegate: ProductListDelegate?
    private var products: [Product]
    private(set) var productCells: [ProductCellPresenter]
    private var orderType: Product.DescriptionKey
    private var orderDirection: OrderDirection

    init(_ viewDelegate: ProductListDelegate?, products: [Product] = [], orderType: Product.DescriptionKey, orderDirection: OrderDirection) {
        self.viewDelegate = viewDelegate
        self.orderType = orderType
        self.orderDirection = orderDirection
        self.products = products
        self.productCells = products.compactMap({ ProductCellPresenter(product: $0) })
        viewDelegate?.updateButton(direction: orderDirection)
        loadProducts()
    }

    private func loadProducts() {
        APIService.getProducts { [weak self] (result) in
            switch result {
            case .success(let products):
                guard let self = self else { return }
                self.products = products
                self.sortProducts()
            case .failure(let error):
                print(error)
            }
        }
    }

    private func sortProducts() {
        self.products = products.sorted(by: { isGreater(first: $0, second: $1) })
        productCells = products.compactMap({ ProductCellPresenter(product: $0) })
        self.viewDelegate?.reloadData()
    }

    private func isGreater(first: Product, second: Product) -> Bool {
        var isGreater: Bool = false
        switch orderType {
        case .gtin14:
            isGreater = (first.gtin14 ?? "") > (second.gtin14 ?? "")
        case .name:
            isGreater = (first.name ?? "") > (second.name ?? "")
        case .author:
            isGreater = (first.author ?? "") > (second.author ?? "")
        case .format:
            isGreater = (first.format?.rawValue ?? "") > (second.format?.rawValue ?? "")
        case .publisher:
            isGreater = (first.publisher ?? "") > (second.publisher ?? "")
        case .brandName:
            isGreater = (first.publisher ?? "") > (second.publisher ?? "")
        case .size:
            isGreater = (first.size ?? "") > (second.size ?? "")
        case .servingSize:
            isGreater = (first.servingSize ?? "") > (second.servingSize ?? "")
        case .servingsPerContainer:
            isGreater = (first.servingsPerContainer ?? "") > (second.servingsPerContainer ?? "")
        case .pages:
            isGreater = (first.pages ?? 0) > (second.pages ?? 0)
        case .calories:
            isGreater = (first.calories ?? 0) > (second.calories ?? 0)
        case .fatCalories:
            isGreater = (first.fatCalories ?? 0) > (second.fatCalories ?? 0)
        case .fat:
            isGreater = (first.fat ?? 0) > (second.fat ?? 0)
        case .saturatedFat:
            isGreater = (first.saturatedFat ?? 0) > (second.saturatedFat ?? 0)
        case .transFat:
            isGreater = (first.transFat ?? 0) > (second.transFat ?? 0)
        case .polyunsaturatedFat:
            isGreater = (first.polyunsaturatedFat ?? 0) > (second.polyunsaturatedFat ?? 0)
        case .monounsaturatedFat:
            isGreater = (first.monounsaturatedFat ?? 0) > (second.monounsaturatedFat ?? 0)
        case .cholesterol:
            isGreater = (first.cholesterol ?? 0) > (second.cholesterol ?? 0)
        case .sodium:
            isGreater = (first.sodium ?? 0) > (second.sodium ?? 0)
        case .carbohydrate:
            isGreater = (first.carbohydrate ?? 0) > (second.carbohydrate ?? 0)
        case .fiber:
            isGreater = (first.fiber ?? 0) > (second.fiber ?? 0)
        case .sugars:
            isGreater = (first.sugars ?? 0) > (second.sugars ?? 0)
        case .protein:
            isGreater = (first.protein ?? 0) > (second.protein ?? 0)
        case .potassium:
            isGreater = (first.potassium ?? 0) > (second.potassium ?? 0)
        }
        return orderDirection == .descending ? isGreater : !isGreater
    }

}

// MARK: Public

extension ProductListPresenter {

    func changeOrderDirection() {
        orderDirection = orderDirection == .ascending ? .descending : .ascending
        sortProducts()
        viewDelegate?.updateButton(direction: orderDirection)
    }

    func setOrderType(rawValue: String) {
        orderType = Product.DescriptionKey(rawValue: rawValue) ?? .gtin14
        sortProducts()
    }

    func getOrderedTypes() -> [String] {
        return Product.DescriptionKey.allCases.map{ $0.rawValue }
    }
}
