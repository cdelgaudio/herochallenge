//
//  Product.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import Foundation

struct Product: Codable {
    enum DescriptionKey: String, CaseIterable {
        case gtin14
        case name
        case author
        case format
        case publisher
        case brandName
        case size
        case servingSize
        case servingsPerContainer
        case pages
        case calories
        case fatCalories
        case fat
        case saturatedFat
        case transFat
        case polyunsaturatedFat
        case monounsaturatedFat
        case cholesterol
        case sodium
        case carbohydrate
        case fiber
        case sugars
        case protein
        case potassium
    }

    enum CoverFormat: String, Codable {
        case hardcover = "Hardcover"
        case paperback = "Paperback"
    }

    let gtin14, name, author: String?
    let format: CoverFormat?
    let publisher: String?
    let pages: Int?
    let brandName, size, servingSize, servingsPerContainer: String?
    let calories, fatCalories: Double?
    let fat, saturatedFat: Double?
    let transFat, polyunsaturatedFat: Double?
    let monounsaturatedFat: Double?
    let cholesterol, sodium, carbohydrate, fiber: Double?
    let sugars, protein, potassium: Double?

    enum CodingKeys: String, CodingKey {
        case gtin14, name, author, format, publisher, pages
        case brandName = "brand_name"
        case size
        case servingSize = "serving_size"
        case servingsPerContainer = "servings_per_container"
        case calories
        case fatCalories = "fat_calories"
        case fat
        case saturatedFat = "saturated_fat"
        case transFat = "trans_fat"
        case polyunsaturatedFat = "polyunsaturated_fat"
        case monounsaturatedFat = "monounsaturated_fat"
        case cholesterol, sodium, carbohydrate, fiber, sugars, protein, potassium
    }
}
