//
//  ProductCell.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var backgroundCell: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundCell.layer.cornerRadius = 10
        backgroundCell.backgroundColor = .heroLightBlue
    }
    func setup(presenter: ProductCellPresenter?) {
        descriptionLabel.text = presenter?.getDescription()
    }
}

extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}

extension UIColor {
    class var heroLightBlue: UIColor {
        return #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 0.3420109161)
    }
}
