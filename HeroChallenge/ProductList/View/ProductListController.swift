//
//  ViewController.swift
//  HeroChallenge
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import UIKit

class ProductListController: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var orderDirectionButton: UIButton!
    @IBOutlet private weak var orderTypeTextfield: PickableTextfield!

    var presenter: ProductListPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProductListPresenter(self, orderType: .gtin14, orderDirection: .descending)
        orderTypeTextfield.text = Product.DescriptionKey.gtin14.rawValue
        orderTypeTextfield.setup(elements: presenter?.getOrderedTypes() ?? []) { [weak self] selected in
            self?.presenter?.setOrderType(rawValue: selected ?? "")
        }
    }

    @IBAction func orderDirectionButtonTapped(_ sender: UIButton) {
        presenter?.changeOrderDirection()
    }

}

extension ProductListController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.productCells.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductCell.identifier) as? ProductCell
        cell?.setup(presenter: presenter?.productCells[indexPath.row])
        return cell ?? UITableViewCell()
    }
}

extension ProductListController: ProductListDelegate {
    func updateButton(direction: OrderDirection) {
        orderDirectionButton.setTitle(direction.rawValue.capitalized, for: .normal)
    }

    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
