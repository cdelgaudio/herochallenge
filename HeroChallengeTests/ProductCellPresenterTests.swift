//
//  ProductCellPresenter.swift
//  HeroChallengeTests
//
//  Created by Carmine on 24/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import XCTest
@testable import HeroChallenge

class ProductCellPresenterTests: XCTestCase {

    func testInitProductCellPresenter() {
        let product = Product(gtin14: "Test", name: nil, author: nil, format: nil, publisher: nil, pages: nil, brandName: nil, size: nil, servingSize: nil, servingsPerContainer: nil, calories: nil, fatCalories: nil, fat: nil, saturatedFat: nil, transFat: nil, polyunsaturatedFat: nil, monounsaturatedFat: nil, cholesterol: nil, sodium: nil, carbohydrate: nil, fiber: nil, sugars: nil, protein: nil, potassium: nil)
        let presenter = ProductCellPresenter(product: product)
        XCTAssert(presenter.getDescription() == "Gtin14: Test", "Init Failed")
    }

}
