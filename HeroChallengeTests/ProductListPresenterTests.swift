//
//  HeroChallengeTests.swift
//  HeroChallengeTests
//
//  Created by Carmine on 23/09/2019.
//  Copyright © 2019 Carmine. All rights reserved.
//

import XCTest
@testable import HeroChallenge

class ProductListPresenterTests: XCTestCase {

    func testInitProductListPresenter() {
        let product = Product(gtin14: "Test", name: nil, author: nil, format: nil, publisher: nil, pages: nil, brandName: nil, size: nil, servingSize: nil, servingsPerContainer: nil, calories: nil, fatCalories: nil, fat: nil, saturatedFat: nil, transFat: nil, polyunsaturatedFat: nil, monounsaturatedFat: nil, cholesterol: nil, sodium: nil, carbohydrate: nil, fiber: nil, sugars: nil, protein: nil, potassium: nil)
        let presenter = ProductListPresenter(nil, products: [product], orderType: .gtin14, orderDirection: .descending)
        XCTAssert(presenter.productCells.map({$0.getDescription() }) == ["Gtin14: Test"], "Init Failed")
    }

    func testChangeOrderProductListPresenter() {
        let product1 = Product(gtin14: "1", name: nil, author: nil, format: nil, publisher: nil, pages: nil, brandName: nil, size: nil, servingSize: nil, servingsPerContainer: nil, calories: nil, fatCalories: nil, fat: nil, saturatedFat: nil, transFat: nil, polyunsaturatedFat: nil, monounsaturatedFat: nil, cholesterol: nil, sodium: nil, carbohydrate: nil, fiber: nil, sugars: nil, protein: nil, potassium: nil)
        let product2 = Product(gtin14: "2", name: nil, author: nil, format: nil, publisher: nil, pages: nil, brandName: nil, size: nil, servingSize: nil, servingsPerContainer: nil, calories: nil, fatCalories: nil, fat: nil, saturatedFat: nil, transFat: nil, polyunsaturatedFat: nil, monounsaturatedFat: nil, cholesterol: nil, sodium: nil, carbohydrate: nil, fiber: nil, sugars: nil, protein: nil, potassium: nil)
        let presenter = ProductListPresenter(nil, products: [product1, product2], orderType: .gtin14, orderDirection: .ascending)
        XCTAssert(presenter.productCells.map({$0.getDescription() }) == ["Gtin14: 1", "Gtin14: 2"], "wrong order")
        presenter.changeOrderDirection()
        XCTAssert(presenter.productCells.map({$0.getDescription() }) == ["Gtin14: 2", "Gtin14: 1"], "order not changed")
    }

    func testSetOrderTypeProductListPresenter() {
        let product1 = Product(gtin14: "1", name: "test2", author: nil, format: nil, publisher: nil, pages: nil, brandName: nil, size: nil, servingSize: nil, servingsPerContainer: nil, calories: nil, fatCalories: nil, fat: nil, saturatedFat: nil, transFat: nil, polyunsaturatedFat: nil, monounsaturatedFat: nil, cholesterol: nil, sodium: nil, carbohydrate: nil, fiber: nil, sugars: nil, protein: nil, potassium: nil)
        let product2 = Product(gtin14: "2", name: "test1", author: nil, format: nil, publisher: nil, pages: nil, brandName: nil, size: nil, servingSize: nil, servingsPerContainer: nil, calories: nil, fatCalories: nil, fat: nil, saturatedFat: nil, transFat: nil, polyunsaturatedFat: nil, monounsaturatedFat: nil, cholesterol: nil, sodium: nil, carbohydrate: nil, fiber: nil, sugars: nil, protein: nil, potassium: nil)
        let presenter = ProductListPresenter(nil, products: [product1, product2], orderType: .gtin14, orderDirection: .ascending)
        XCTAssert(presenter.productCells.first?.getDescription() == "Gtin14: 1\nName: test2", "wrong order")
        presenter.setOrderType(rawValue: "name")
        XCTAssert(presenter.productCells.first?.getDescription() == "Gtin14: 2\nName: test1", "order not changed")
    }

}
